#include <iostream>
#include <fstream>
#include "core/sprite.hpp"
#include "image/image.hpp"
#include "core/packer.hpp"
#include "writer/writer.hpp"
#include "clipp/clipp.h"
#include "ThreadPool/ThreadPool.h"

struct ParseResult {
    core::Parameters parameters{};
    std::vector<fs::path> inputPaths{};
    std::string descriptionTemplate{};
    fs::path outputPath{};
    writer::ImageType outputFormat{};
    std::string nameTemplate{};
    unsigned alphaThreshold{};
};

ParseResult parseCommandLineArguments(int argc, char **argv) {
    ParseResult result{};

    std::vector<std::string> inputValue;
    std::string outputValue, formatValue, descriptionValue, nameValue = "{{index}}";

    bool POT{false}, rotate{false};
    int sizeX{0}, sizeY{0}, maxSizeX{8192}, maxSizeY{8192}, padX{1}, padY{1}, threshold{0};

    using namespace clipp;

    auto cli = (
        (required("--input", "-i") & values("input_paths", inputValue)) % "Paths for input images, if path is directory then all containing images will be added, if file then it will be added",
        (required("--output", "-o") & value("output_path", outputValue)) % "Directory where atlases will be placed",
        (required("--format", "-f") & value("texture_format", formatValue)) % "Output image format, can be of: [png, tga]",
        (required("--description", "-d") & value("template_file", descriptionValue)) % "File containing template of description, use {{ mustache }} for templates",
        option("--POT", "-2").set(POT) % "Create atlas textures with Power Of Two size values (default: false)",
        option("--rotate", "-r").set(rotate) % "Enable rotation of sprites on atlases (default: false)",
        (option("--size", "-s") & value("size_x", sizeX) & value("size_y", sizeY)) % "Restrict size of atlases with provided values (default: not set)",
        (option("--max-size", "-m") & value("max_size_x", maxSizeX) & value("max_size_y", maxSizeY)) % "Restrict size of atlases with provided values (default: 8192 8192)",
        (option("--pad", "-p") & value("padding_x", padX) & value("padding_y", padY)) % "Pad sprites on atlases with this values (default: 1 1)",
        (option("--name", "-n") & value("name_template", nameValue)) % "Template for names of output files (default: \"{{index}}\")",
        (option("--threshold", "-t") & value("alpha_threshold", threshold)) % "Alpha threshold for images [0-255] (default: 0)"
    );

    auto fmt = doc_formatting{}.first_column(3).last_column(100);

    auto parseResult = parse(argc, argv, cli);
    if (!parseResult) {
        for (auto item : parseResult.missing()) {
            auto flags = item.param()->flags();
            auto label = item.param()->label();
            if (!flags.empty()) {
                std::cerr << "option ( ";
                for (const auto &flag : flags) {
                    std::cerr << flag << ' ';
                }
                std::cerr << ") is missing" << std::endl;
            } else if (!label.empty()) {
                std::cerr << "value ( " << label << " ) is missing" << std::endl;
            }
        }
        std::cout << make_man_page(cli, argv[0], fmt);
        std::exit(1);
    }

    for (const auto &inputPath : inputValue) {
        if (!fs::exists(inputPath)) {
            std::cerr << "input path " << inputPath << " does not exist" << std::endl;
            std::exit(1);
        }
    }
    result.inputPaths.reserve(inputValue.size());
    for (const auto &inputPath : inputValue) {
        result.inputPaths.emplace_back(fs::path(inputPath));
    }

    result.outputPath = fs::path(outputValue);
    if (!fs::exists(result.outputPath)) {
        std::cout << "output path " << outputValue << " does not exist, it will be created" << std::endl;
        try {
            fs::create_directory(result.outputPath);
        }
        catch (const fs::filesystem_error &e) {
            std::cerr << e.what() << std::endl;
            std::exit(1);
        }
    } else if (!fs::is_directory(result.outputPath)) {
        std::cerr << "output path " << outputValue << " is not a directory" << std::endl;
        std::exit(1);
    }

    std::transform(formatValue.begin(), formatValue.end(), formatValue.begin(), ::tolower);
    if (formatValue == "png") {
        result.outputFormat = writer::ImageType::PNG;
    } else if (formatValue == "tga") {
        result.outputFormat = writer::ImageType::TGA;
    } else {
        std::cerr << "format is wrong" << std::endl;
        std::cout << make_man_page(cli, argv[0], fmt) << std::endl;
        std::exit(1);
    }

    auto templatePath = fs::path(descriptionValue);
    if (!fs::exists(templatePath)) {
        std::cerr << "description template file " << descriptionValue << " does not exist" << std::endl;
        std::exit(1);
    } else {
        std::ifstream templateFile(templatePath, std::ios::in);
        if (!templateFile) {
            std::cerr << "failed to read description template file " << descriptionValue << std::endl;
            std::exit(1);
        }
        result.descriptionTemplate = {std::istreambuf_iterator<char>(templateFile), std::istreambuf_iterator<char>()};
    }

    result.parameters.POT = POT;
    result.parameters.shouldRotate = rotate;

    if (sizeX < 0 || sizeY < 0) {
        std::cerr << "size is invalid" << std::endl;
        std::exit(1);
    } else if (sizeX == 0 && sizeY == 0) {
        result.parameters.isSizeFinite = true;
        if (maxSizeX < 0 || maxSizeY < 0) {
            std::cerr << "size is invalid" << std::endl;
            std::exit(1);
        } else if (maxSizeX != 0 && maxSizeY != 0) {
            result.parameters.size = primitives::Size{static_cast<unsigned>(maxSizeX), static_cast<unsigned>(maxSizeY)};
        }
    } else {
        result.parameters.isSizeFinite = false;
        result.parameters.size = primitives::Size{static_cast<unsigned>(sizeX), static_cast<unsigned>(sizeY)};
    }

    if (padX < 0 || padY < 0) {
        std::cerr << "padding is invalid" << std::endl;
        std::exit(1);
    } else {
        result.parameters.padding = {static_cast<unsigned>(padX), static_cast<unsigned>(padY)};
    }

    if (nameValue.empty()) {
        std::cerr << "name template is empty" << std::endl;
        std::exit(1);
    } else {
        result.nameTemplate = nameValue;
    }

    if (threshold < 0 || threshold > 255) {
        std::cerr << "alpha threshold is invalid" << std::endl;
        std::exit(1);
    } else {
        result.alphaThreshold = static_cast<unsigned>(threshold);
    }

    return result;
}

int main(int argc, char **argv) {
    auto parseResult = parseCommandLineArguments(argc, argv);
    core::Packer packer(parseResult.parameters);

    std::vector<fs::path> inputPaths;

    for (const auto &inputPath : parseResult.inputPaths) {
        if (fs::is_regular_file(inputPath)) {
            inputPaths.emplace_back(inputPath);
        } else if (fs::is_directory(inputPath)) {
            for (auto &item: fs::recursive_directory_iterator(inputPath)) {
                if (fs::is_regular_file(item)) {
                    inputPaths.emplace_back(item);
                }
            }
        }
    }

    std::vector<std::future<std::shared_ptr<core::Sprite>>> results;
    results.reserve(inputPaths.size());
    {
        auto pool = ThreadPool(std::min<size_t>(inputPaths.size(), std::thread::hardware_concurrency()));
        for (const auto &path : inputPaths) {
            results.emplace_back(
                    pool.enqueue([&path, &parseResult] {
                        auto image = image::loadImage(path);
                        return std::make_shared<core::Sprite>(path.stem(), image, parseResult.alphaThreshold);
                    })
            );
        }
    }

    for (auto &result : results) {
        packer.addSprite(result.get());
    }

    auto res = packer.getResult();

    writer::writeAtlases(res, parseResult.descriptionTemplate, parseResult.nameTemplate, parseResult.outputFormat, parseResult.outputPath);

    return 0;
}
