#include "writer/writer.hpp"

#include <utility>
#include <thread>

#define STB_IMAGE_WRITE_IMPLEMENTATION

#include "stb/stb_image_write.h"
#include "fmt/format.h"
#include "mstch/mstch.hpp"
#include "ThreadPool/ThreadPool.h"

using namespace fmt::literals;

namespace writer {

    namespace {

        std::string getAtlasName(unsigned index, const core::Atlas &atlas, const std::string &nameTemplate) {
            auto atlasTextureSize = atlas.getSize();
            mstch::map atlasExportInfo{
                    {"texture_width",  atlasTextureSize.width},
                    {"texture_height", atlasTextureSize.height},
                    {"index",          index}
            };
            return mstch::render(nameTemplate, atlasExportInfo);
        }

        void createAtlasImage(const core::Atlas &atlas, std::string atlasName, const fs::path &directory, ImageType type) {
            auto size = atlas.getSize();
            auto buffer = std::vector<unsigned char>(size.width * size.height * 4);

            for (auto &spriteInfo : atlas) {
                if (spriteInfo->isAlias()) {
                    continue;
                }

                auto pos = spriteInfo->getPosition();
                auto isRotated = spriteInfo->isRotated();
                auto area = spriteInfo->getArea(true);
                auto image = spriteInfo->getImage();

                if (area.width == 0u || area.height == 0u) {
                    continue;
                }

                for (unsigned x = 0; x < area.width; ++x) {
                    for (unsigned y = 0; y < area.height; ++y) {
                        auto sourceX = x + area.x;
                        auto sourceY = y + area.y;
                        auto destinationX = (isRotated ? y : x) + pos.x;
                        auto destinationY = (isRotated ? x : y) + pos.y;

                        auto sourcePixel = image->getPixel(sourceX, sourceY);
                        auto destinationPixel = buffer.data() + (destinationX + destinationY * size.width) * 4;

                        destinationPixel[0] = sourcePixel[0];
                        destinationPixel[1] = sourcePixel[1];
                        destinationPixel[2] = sourcePixel[2];
                        destinationPixel[3] = sourcePixel[3];
                    }
                }
            }

            switch (type) {
                case ImageType::PNG: {
                    atlasName.append(".png");
                    auto res = stbi_write_png((directory / atlasName).c_str(), static_cast<int>(size.width), static_cast<int>(size.height), 4, buffer.data(), 0);
                    if (res == 0) {
                        throw std::runtime_error("atlas image ({}) saving failed"_format(atlasName.c_str()));
                    }
                    break;
                }
                case ImageType::TGA: {
                    atlasName.append(".tga");
                    auto res = stbi_write_tga((directory / atlasName).c_str(), static_cast<int>(size.width), static_cast<int>(size.height), 4, buffer.data());
                    if (res == 0) {
                        throw std::runtime_error("atlas image ({}) saving failed"_format(atlasName.c_str()));
                    }
                    break;
                }
            }

        }

        void createAtlasDescription(const core::Atlas &atlas, std::string atlasName, const fs::path &directory, const std::string &descriptionTemplate) {
            auto atlasTextureSize = atlas.getSize();

            auto descriptionExtension = ".description";
            atlasName.append(descriptionExtension);
            auto descriptionPath = directory / atlasName;

            FILE *descriptionFile = std::fopen(descriptionPath.c_str(), "w");
            if (!descriptionFile) {
                throw std::runtime_error("atlas description ({}) saving failed with system error: {}"_format(atlasName.c_str(), std::strerror(errno)));
            }

            auto sprites = atlas.getSprites();
            std::sort(sprites.begin(), sprites.end(),
                      [](const std::shared_ptr<core::ISpriteInfo> &left, const std::shared_ptr<core::ISpriteInfo> &right) {
                          return left->getName() < right->getName();
                      });

            mstch::array spritesExportInfo;

            for (size_t i = 0u, ie = sprites.size(); i < ie; ++i) {
                const auto &spriteInfo = sprites[i];
                auto pos = spriteInfo->getPosition();
                auto isRotated = spriteInfo->isRotated();
                auto &name = spriteInfo->getName();
                auto trimmedArea = spriteInfo->getArea(true);
                auto originalArea = spriteInfo->getArea(false);
                auto originalCenter = primitives::Point<double>{static_cast<double>(originalArea.width) / 2.0, static_cast<double>(originalArea.height) / 2.0};
                auto trimmedCenter = primitives::Point<double>{originalCenter.x - trimmedArea.x, originalCenter.y - trimmedArea.y};

                mstch::map spriteExportInfo{
                        {"name",             name},
                        {"pos_x",            pos.x},
                        {"pos_y",            pos.y},
                        {"rotated",          isRotated},
                        {"trimmed_x",        trimmedArea.x},
                        {"trimmed_y",        trimmedArea.y},
                        {"trimmed_width",    trimmedArea.width},
                        {"trimmed_height",   trimmedArea.height},
                        {"trimmed_center_x", trimmedCenter.x},
                        {"trimmed_center_y", trimmedCenter.y},
                        {"center_x",         originalCenter.x},
                        {"center_y",         originalCenter.y},
                        {"first",            i == 0u},
                        {"last",             i == ie - 1u},
                };

                spritesExportInfo.emplace_back(std::move(spriteExportInfo));
            }

            mstch::map atlasExportInfo{
                    {"texture_width",  atlasTextureSize.width},
                    {"texture_height", atlasTextureSize.height},
                    {"sprites",        spritesExportInfo}
            };

            auto description = mstch::render(descriptionTemplate, atlasExportInfo);

            int res;
            res = std::fprintf(descriptionFile, "%s", description.c_str());
            if (res < 0 || res != static_cast<int>(description.size())) {
                throw std::runtime_error("atlas description ({}) writing failed with system error: {}"_format(atlasName.c_str(), std::strerror(errno)));
            }
            res = std::fclose(descriptionFile);
            if (res != 0) {
                throw std::runtime_error("atlas description ({}) saving failed with system error: {}"_format(atlasName.c_str(), std::strerror(errno)));
            }
        }

    }

    void writeAtlases(const std::shared_ptr<core::Result> &result, const std::string &descriptionTemplate, const std::string &nameTemplate, ImageType type, const fs::path &directory) {
        unsigned atlasIndex = 0u;
        std::string lastAtlasName;

        std::vector<std::future<void>> results;
        results.reserve(result->getAtlasesAmount());
        {
            auto pool = ThreadPool(std::min<size_t>(result->getAtlasesAmount(), std::thread::hardware_concurrency()));
            for (auto &atlas : *result) {
                auto atlasName = getAtlasName(atlasIndex, atlas, nameTemplate);
                if (atlasName.empty()) {
                    throw std::runtime_error("atlas name is empty for index {}"_format(atlasIndex));
                } else if (atlasName == lastAtlasName) {
                    throw std::runtime_error("atlas name ({}) is same as previous atlas name for index {}"_format(atlasName, atlasIndex));
                }
                results.emplace_back(
                        pool.enqueue([&atlas, atlasName, &directory, type, &descriptionTemplate] {
                            createAtlasImage(atlas, atlasName, directory, type);
                            createAtlasDescription(atlas, atlasName, directory, descriptionTemplate);
                        }));
                lastAtlasName = atlasName;
                ++atlasIndex;
            }
        }
    }

}
