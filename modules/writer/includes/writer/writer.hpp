#pragma once

#include "core/result.hpp"

namespace writer {

    enum class ImageType {
        PNG,
        TGA
    };

    void writeAtlases(const std::shared_ptr<core::Result> &result, const std::string &descriptionTemplate, const std::string &nameTemplate, ImageType type, const fs::path &directory);

}
