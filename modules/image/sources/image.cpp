#include "image/image.hpp"

#include <algorithm>
#include <utility>
#include "stb_image_impl.hpp"

namespace image {

    Image::Image(std::string filename)
            : _filename(std::move(filename)),
              _size() {}

    Image::~Image() = default;

    const std::string &Image::getFilename() const noexcept {
        return _filename;
    }

    primitives::Size Image::getSize() const noexcept {
        return _size;
    }

    std::shared_ptr<Image> loadImage(const fs::path &file) {
        if (!fs::exists(file)) {
            throw std::runtime_error("image is not even exists");
        }

        auto extension = file.extension().string();
        std::transform(extension.begin(), extension.end(), extension.begin(), ::tolower);
        if (extension == ".png" || extension == ".tga") {
            return std::make_shared<StbImage>(file);
        } else {
            throw std::runtime_error("image type is not supported");
        }
    }

}
