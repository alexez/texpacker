#include "stb_image_impl.hpp"

#define STB_IMAGE_IMPLEMENTATION

#include "stb/stb_image.h"

namespace image {

    StbImage::StbImage(const fs::path &file)
            : Image(file),
              _image(nullptr),
              _width(0),
              _height(0) {
        int unused, width, height;
        _image = stbi_load(file.c_str(), &width, &height, &unused, 4);
        if (_image == nullptr) {
            throw std::runtime_error("stb failed to load image");
        }
        _width = static_cast<unsigned>(width);
        _height = static_cast<unsigned>(height);
        _size = {_width, _height};
    }

    StbImage::~StbImage() {
        stbi_image_free(_image);
    }

    inline bool isPixelEmpty(const stbi_uc *pixel, uint8_t threshold) {
        return pixel[3] <= threshold;
    }

    inline bool isLineEmpty(const stbi_uc *data, unsigned line, unsigned width, uint8_t threshold) {
        for (unsigned x = 0; x < width; ++x) {
            if (!isPixelEmpty(data + (x + line * width) * 4, threshold)) {
                return false;
            }
        }
        return true;
    }

    inline bool isRowEmpty(const stbi_uc *data, unsigned row, unsigned width, unsigned height, uint8_t threshold) {
        for (unsigned y = 0; y < height; ++y) {
            if (!isPixelEmpty(data + (row + y * width) * 4, threshold)) {
                return false;
            }
        }
        return true;
    }

    primitives::Area StbImage::getTrimmedArea(uint8_t threshold) const {
        if (_width == 0u || _height == 0u) {
            return {0u, 0u, 0u, 0u};
        }
        primitives::Area res{};
        for (unsigned x = 0; x < _width; ++x) {
            if (!isRowEmpty(_image, x, _width, _height, threshold)) {
                res.x = x;
                break;
            }
        }
        for (unsigned x = _width - 1;; --x) {
            if (!isRowEmpty(_image, x, _width, _height, threshold)) {
                res.width = x - res.x + 1;
                break;
            } else if (x == 0) {
                res.width = 0;
                break;
            }
        }
        for (unsigned y = 0; y < _height; ++y) {
            if (!isLineEmpty(_image, y, _width, threshold)) {
                res.y = y;
                break;
            }
        }
        for (unsigned y = _height - 1;; --y) {
            if (!isLineEmpty(_image, y, _width, threshold)) {
                res.height = y - res.y + 1;
                break;
            } else if (y == 0) {
                res.height = 0;
                break;
            }
        }
        return res;
    }

    unsigned char *StbImage::getPixel(unsigned x, unsigned y) const {
        return _image + (x + y * _width) * 4;
    }

}
