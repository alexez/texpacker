#pragma once

#include "image/image.hpp"
#include "stb/stb_image.h"

namespace image {

    class StbImage final : public Image {
    public:
        explicit StbImage(const fs::path &file);
        StbImage(const StbImage &other) = delete;
        StbImage &operator=(const StbImage &other) = delete;
        ~StbImage() override;

        primitives::Area getTrimmedArea(uint8_t threshold) const override;
        unsigned char* getPixel(unsigned x, unsigned y) const override;

    private:
        stbi_uc *_image;
        unsigned _width, _height;
    };

}
