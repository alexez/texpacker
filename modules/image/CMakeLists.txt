project(image LANGUAGES CXX)

add_library(${PROJECT_NAME} STATIC)

target_sources(${PROJECT_NAME} PRIVATE
    includes/image/image.hpp
    sources/image.cpp
        sources/stb_image_impl.hpp
        sources/stb_image_impl.cpp
)

target_include_directories(${PROJECT_NAME}
    PUBLIC includes
    PRIVATE sources
)

target_link_libraries(${PROJECT_NAME} PUBLIC primitives stb filesystem)

target_compile_options(${PROJECT_NAME} PRIVATE ${WARNING_OPTIONS})
