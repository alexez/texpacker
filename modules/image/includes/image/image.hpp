#pragma once

#include <string>
#include <memory>
#include "filesystem/filesystem.hpp"
#include "primitives/primitives.hpp"

namespace image {

    class Image {
    public:
        virtual ~Image();

        const std::string &getFilename() const noexcept;
        primitives::Size getSize() const noexcept;

        virtual primitives::Area getTrimmedArea(uint8_t threshold) const = 0;
        virtual unsigned char* getPixel(unsigned x, unsigned y) const = 0;

    protected:
        explicit Image(std::string filename);

        std::string _filename;
        primitives::Size _size;
    };

    std::shared_ptr<Image> loadImage(const fs::path &file);

}
