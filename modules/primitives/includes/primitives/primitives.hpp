#pragma once

namespace primitives {

    struct Size {
        unsigned width;
        unsigned height;
    };

    template <typename T = unsigned>
    struct Point {
        T x;
        T y;
    };

    struct Area {
        unsigned x;
        unsigned y;
        unsigned width;
        unsigned height;
    };

}
