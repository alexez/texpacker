#include "core/sprite.hpp"

#include <utility>
#include "hash/md5.h"

namespace core {

    Sprite::Sprite(std::string name, std::shared_ptr<image::Image> image, uint8_t threshold)
            : _name(std::move(name)),
              _image(std::move(image)),
              _area(),
              _areaOrig(),
              _hash() {
        auto imageSize = _image->getSize();
        _areaOrig = {0u, 0u, imageSize.width, imageSize.height};
        _area = _image->getTrimmedArea(threshold);
        _hash = computeHash();
    }

    const std::string &Sprite::getName() const noexcept {
        return _name;
    }

    void Sprite::setName(const std::string &name) {
        _name = name;
    }

    const std::shared_ptr<image::Image> &Sprite::getImage() const noexcept {
        return _image;
    }

    primitives::Area Sprite::getArea(bool trimmed) const noexcept {
        return trimmed ? _area : _areaOrig;
    }

    std::string Sprite::getHash() const noexcept {
        return _hash;
    }

    std::string Sprite::computeHash() const {
        auto area = getArea(true);
        auto &image = getImage();

        MD5 md5;
        for (unsigned x = area.x, xe = area.x + area.width; x < xe; ++x) {
            for (unsigned y = area.y, ye = area.y + area.height; y < ye; ++y) {
                md5.add(image->getPixel(x, y), 4);
            }
        }

        return md5.getHash();
    }

}
