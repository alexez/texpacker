#include "core/packer.hpp"

#include <cmath>
#include "ThreadPool/ThreadPool.h"

namespace core {

    Packer::Packer(Parameters parameters)
            : _parameters(parameters),
              _sprites(),
              _emptySprites(),
              _rects(),
              _spriteHashMap(),
              _spriteAliasesMap() {}

    Packer::~Packer() = default;

    void Packer::addSprite(std::shared_ptr<Sprite> sprite) {
        auto spriteArea = sprite->getArea(true);
        auto width = spriteArea.width + _parameters.padding.x;
        auto height = spriteArea.height + _parameters.padding.y;

        if (width == 0u || height == 0u) {
            _emptySprites.emplace_back(std::move(sprite));
            return;
        }

        auto hash = sprite->getHash();
        auto it = _spriteHashMap.find(hash);
        if (it == _spriteHashMap.end()) {
            _spriteHashMap[hash] = sprite.get();
        } else {
            _spriteAliasesMap[it->second].emplace_back(sprite);
            return;
        }

        if (_parameters.size.width > 0 && _parameters.size.height > 0) {
            const auto w = 2 * _parameters.padding.x + spriteArea.width;
            const auto h = 2 * _parameters.padding.y + spriteArea.height;
            if (w > _parameters.size.width || h > _parameters.size.height) {
                throw std::runtime_error("sprite is too big");
            }
        }
        _rects.emplace_back(primitives::Size{width, height});
        _sprites.emplace_back(std::move(sprite));
    }

    std::shared_ptr<Result> Packer::getResult() {
        std::array<rbp::MaxRects::Mode, 5> modes = {
                rbp::MaxRects::Mode::BottomLeft,
                rbp::MaxRects::Mode::ShortSide,
                rbp::MaxRects::Mode::LongSide,
                rbp::MaxRects::Mode::BestArea,
                rbp::MaxRects::Mode::ContactPoint
        };

        if (_sprites.empty()) {
            throw std::runtime_error("unable to create atlas of nothing");
        }

        std::shared_ptr<Result> best;
        uint64_t bestArea = 0;

        std::vector<std::future<std::shared_ptr<core::Result>>> results;
        results.reserve(modes.size());
        {
            auto pool = ThreadPool(std::min<size_t>(modes.size(), std::thread::hardware_concurrency()));
            for (auto &mode : modes) {
                results.emplace_back(
                        pool.enqueue([this, mode] {
                            return getResult(mode);
                        })
                );
            }
        }
        for (auto &result : results) {
            auto res = result.get();

            uint64_t area = 0;

            for (const auto &atlas : *res) {
                area += atlas.getSize().width * atlas.getSize().height;
            }

            if (best == nullptr || res->getAtlasesAmount() < best->getAtlasesAmount() ||
                (res->getAtlasesAmount() == best->getAtlasesAmount() && area < bestArea)) {

                best = std::move(res);
                bestArea = area;
            }
        }

        if (!_emptySprites.empty()) {
            if (best->getAtlasesAmount() == 0) {
                throw std::runtime_error("unable to create atlas of empty sprites");
            }

            auto &atlas = best->getAtlas(0);
            for (const auto &emptySprite : _emptySprites) {
                atlas.addSprite(std::make_shared<PositionedSpriteInfo>(emptySprite, primitives::Point<unsigned>{0u, 0u}, false));
            }
        }

        return best;
    }

    std::shared_ptr<Result> Packer::getResult(rbp::MaxRects::Mode mode) const {
        std::vector<rbp::Rect> resultRects;
        std::vector<size_t> resultIndices;
        std::vector<size_t> inputIndices(_rects.size());
        std::vector<size_t> rectsIndices;
        std::vector<rbp::RectSize> rectSizes(_rects.size());

        for (size_t i = 0; i < _rects.size(); i++) {
            inputIndices[i] = i;
            rectSizes[i] = rbp::RectSize{static_cast<int>(_rects[i].width), static_cast<int>(_rects[i].height)};
        }

        auto size = getInitialSize(inputIndices);
        auto result = std::make_shared<Result>();

        while (!inputIndices.empty()) {
            rectsIndices = inputIndices;

            rbp::MaxRects packer(static_cast<int>(size.width - _parameters.padding.x),
                                 static_cast<int>(size.height - _parameters.padding.y),
                                 _parameters.shouldRotate);
            packer.insert(mode, rectSizes, rectsIndices, resultRects, resultIndices);

            bool addResult = false;

            if (!rectsIndices.empty()) {
                if (!_parameters.isMaximum(size.width, size.height)) {
                    enlargeSize(size);
                } else {
                    addResult = true;
                    inputIndices.swap(rectsIndices);
                }
            } else {
                if (size.width == size.height) { // check size halved by width if result is squared
                    auto halfSize = primitives::Size{size.width / 2u, size.height};

                    bool isValidSize = true;
                    for (auto rect : _rects) {
                        if (rect.width > halfSize.width - _parameters.padding.x * 2u) {
                            isValidSize = false;
                        }
                    }

                    if (isValidSize) {
                        std::vector<rbp::Rect> tempResultRects;
                        std::vector<size_t> tempResultIndices;
                        std::vector<size_t> tempRectsIndices;

                        tempRectsIndices = inputIndices;

                        rbp::MaxRects tempPacker(static_cast<int>(halfSize.width - _parameters.padding.x),
                                                 static_cast<int>(halfSize.height - _parameters.padding.y),
                                                 _parameters.shouldRotate);
                        tempPacker.insert(mode, rectSizes, tempRectsIndices, tempResultRects, tempResultIndices);

                        if (tempRectsIndices.empty()) {
                            resultRects = tempResultRects;
                            resultIndices = tempResultIndices;
                            size = halfSize;
                        }
                    }
                }
                addResult = true;
                inputIndices.clear();
            }

            if (addResult) {
                std::vector<primitives::Area> resultAreas;
                resultAreas.reserve(resultRects.size());
                for (auto rect : resultRects) {
                    auto area = primitives::Area{static_cast<unsigned>(rect.x), static_cast<unsigned>(rect.y),
                                                 static_cast<unsigned>(rect.width), static_cast<unsigned>(rect.height)};
                    resultAreas.emplace_back(area);
                }
                result->addAtlas(getPreparedAtlas(resultAreas, resultIndices, size));

                if (!inputIndices.empty()) {
                    size = getInitialSize(inputIndices);
                }
            }
        }

        return result;
    }

    primitives::Size Packer::getInitialSize(const std::vector<size_t> &rectIndices) const {
        if (_parameters.hasFixedSize()) {
            return _parameters.size;
        }

        uint64_t area = 0;
        for (unsigned long rectIndex : rectIndices) {
            const auto &rc = _rects[rectIndex];
            area += rc.width * rc.height;
        }

        auto size = static_cast<unsigned>(std::ceil(std::sqrt(area)));
        unsigned n = 1u;
        while (n < size) {
            n <<= 1u;
        }

        primitives::Size result{n, n};
        if (_parameters.isSizeFinite) {
            result.width = std::min(n, _parameters.size.width);
            result.height = std::min(n, _parameters.size.height);
        }
        return result;
    }

    Atlas Packer::getPreparedAtlas(const std::vector<primitives::Area> &resultRects, const std::vector<size_t> &resultIndices, primitives::Size size) const {
        Atlas atlas = Atlas(size.width, size.height, resultRects.size());

        unsigned xMin = size.width;
        unsigned xMax = 0;
        unsigned yMin = size.height;
        unsigned yMax = 0;

        for (size_t i = 0; i < resultRects.size(); i++) {
            size_t index = resultIndices[i];

            const auto &baseRect = _rects[index];
            const auto &baseSprite = _sprites[index];
            bool hasAliases = _spriteAliasesMap.find(baseSprite.get()) != _spriteAliasesMap.end();

            auto position = primitives::Point<unsigned>{resultRects[i].x + _parameters.padding.x, resultRects[i].y + _parameters.padding.y};
            auto isRotated = resultRects[i].width != baseRect.width;

            auto positionedSpriteInfo = std::make_shared<PositionedSpriteInfo>(baseSprite, position, isRotated);
            if (hasAliases) {
                for (const auto &alias : _spriteAliasesMap.at(baseSprite.get())) {
                    atlas.addSprite(std::make_shared<AliasSpriteInfo>(positionedSpriteInfo, alias));
                }
            }
            atlas.addSprite(positionedSpriteInfo);

            xMin = std::min(xMin, resultRects[i].x);
            xMax = std::max(xMax, resultRects[i].x + resultRects[i].width);
            yMin = std::min(yMin, resultRects[i].y);
            yMax = std::max(yMax, resultRects[i].y + resultRects[i].height);
        }

        if (!_parameters.hasFixedSize()) {
            if (xMin > _parameters.padding.x || yMin > _parameters.padding.y) {
                for (auto &sprite : atlas) {
                    if (!sprite->isAlias()) {
                        auto positionedInfo = dynamic_cast<PositionedSpriteInfo *>(sprite.get());
                        positionedInfo->updatePosition({sprite->getPosition().x - xMin, sprite->getPosition().y - yMin});
                    }
                    sprite->getPosition();
                }
            }

            if (_parameters.POT) {
                auto atlasWidth = atlas.getSize().width;
                auto atlasHeight = atlas.getSize().height;

                while (atlasWidth >> 1u >= xMax + _parameters.padding.x) {
                    atlasWidth >>= 1u;
                }
                while (atlasHeight >> 1u >= yMax + _parameters.padding.y) {
                    atlasHeight >>= 1u;
                }
                atlas.setSize(atlasWidth, atlasHeight);
            } else {
                atlas.setSize((xMax - xMin) + _parameters.padding.x, (yMax - yMin) + _parameters.padding.y);
            }
        }
        return atlas;
    }

    void Packer::enlargeSize(primitives::Size &size) const {
        if (_parameters.isSizeFinite) {
            bool isWidth;

            if (size.width > size.height) {
                isWidth = size.height >= _parameters.size.height;
            } else {
                isWidth = size.width < _parameters.size.width;
            }

            if (isWidth) {
                size.width = std::min(size.width * 2, _parameters.size.width);
            } else {
                size.height = std::min(size.height * 2, _parameters.size.height);
            }
        } else {
            if (size.width > size.height) {
                size.height = size.height * 2;
            } else {
                size.width = size.width * 2;
            }
        }
    }

}
