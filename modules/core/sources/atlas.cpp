#include "core/atlas.hpp"

namespace core {

    Atlas::Atlas(unsigned width, unsigned height, size_t spritesAmount)
            : _size({width, height}),
              _sprites() {
        _sprites.reserve(spritesAmount);
    }

    Atlas::~Atlas() = default;

    Atlas::Atlas(Atlas &&other) noexcept
            : _size(other._size),
              _sprites(std::move(other._sprites)) {
    }

    Atlas &Atlas::operator=(Atlas &&other) noexcept {
        _size = other._size;
        _sprites = std::move(other._sprites);
        return *this;
    }

    void Atlas::setSize(unsigned width, unsigned height) {
        _size = primitives::Size{width, height};
    }

    primitives::Size Atlas::getSize() const {
        return _size;
    }

    size_t Atlas::getSpritesAmount() const {
        return _sprites.size();
    }

    const ISpriteInfo &Atlas::getSprite(size_t index) const {
        return *_sprites[index];
    }

    ISpriteInfo &Atlas::getSprite(size_t index) {
        return *_sprites[index];
    }

    void Atlas::addSprite(std::shared_ptr<ISpriteInfo> &&sprite) {
        _sprites.emplace_back(std::move(sprite));
    }

}
