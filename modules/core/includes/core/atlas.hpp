#pragma once

#include <vector>
#include "core/sprite.hpp"
#include "primitives/primitives.hpp"
#include "core/sprite_info.hpp"

namespace core {

    class Atlas {
    public:
        Atlas(unsigned width, unsigned height, size_t spritesAmount);
        ~Atlas();

        Atlas(const Atlas &other) = delete;
        Atlas &operator=(const Atlas &other) = delete;

        Atlas(Atlas &&other) noexcept;
        Atlas &operator=(Atlas &&other) noexcept;

        void setSize(unsigned width, unsigned height);
        primitives::Size getSize() const;

        size_t getSpritesAmount() const;
        const ISpriteInfo &getSprite(size_t index) const;
        ISpriteInfo &getSprite(size_t index);
        void addSprite(std::shared_ptr<ISpriteInfo> &&sprite);

        auto begin() noexcept { return _sprites.begin().operator->(); }
        auto end() noexcept { return _sprites.end().operator->(); }
        auto begin() const noexcept { return _sprites.begin().operator->(); }
        auto end() const noexcept { return _sprites.end().operator->(); }

        const auto &getSprites() const noexcept { return _sprites; }

    private:
        primitives::Size _size;
        std::vector<std::shared_ptr<ISpriteInfo>> _sprites;
    };

}
