#pragma once

#include <memory>
#include "image/image.hpp"
#include "primitives/primitives.hpp"
#include "core/comparator.hpp"

namespace core {

    class Sprite {
    public:
        Sprite(std::string name, std::shared_ptr<image::Image> image, uint8_t threshold);

        const std::string &getName() const noexcept;
        void setName(const std::string &name);

        const std::shared_ptr<image::Image> &getImage() const noexcept;

        primitives::Area getArea(bool trimmed) const noexcept;

        std::string getHash() const noexcept;

    private:
        std::string computeHash() const;

        std::string _name;
        std::shared_ptr<image::Image> _image;
        primitives::Area _area, _areaOrig;
        std::string _hash;

    public:
        using Comp = Comparator<Sprite, decltype(&Sprite::_name), &Sprite::_name>;
    };

}
