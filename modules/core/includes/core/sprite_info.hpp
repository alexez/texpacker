#include <utility>

#pragma once

#include <utility>
#include "core/sprite.hpp"

namespace core {

    struct ISpriteInfo {
        virtual ~ISpriteInfo() = default;

        virtual primitives::Point<unsigned> getPosition() const noexcept = 0;
        virtual bool isRotated() const noexcept = 0;
        virtual const std::string &getName() const noexcept = 0;
        virtual primitives::Area getArea(bool trimmed) const noexcept = 0;
        virtual const std::shared_ptr<image::Image> &getImage() const noexcept = 0;
        virtual bool isAlias() const noexcept = 0;
    };

    class PositionedSpriteInfo final : public ISpriteInfo {
    public:
        PositionedSpriteInfo(std::shared_ptr<Sprite> sprite, primitives::Point<unsigned> position, bool isRotated)
                : _sprite(std::move(sprite)),
                  _position(position),
                  _isRotated(isRotated) {}

        primitives::Point<unsigned int> getPosition() const noexcept override {
            return _position;
        }

        bool isRotated() const noexcept override {
            return _isRotated;
        }

        const std::string &getName() const noexcept override {
            return _sprite->getName();
        }

        primitives::Area getArea(bool trimmed) const noexcept override {
            return _sprite->getArea(trimmed);
        }

        const std::shared_ptr<image::Image> &getImage() const noexcept override {
            return _sprite->getImage();
        }

        bool isAlias() const noexcept override {
            return false;
        }

        void updatePosition(primitives::Point<unsigned> newPosition) {
            _position = newPosition;
        }

    private:
        std::shared_ptr<Sprite> _sprite;
        primitives::Point<unsigned> _position;
        bool _isRotated;
    };

    class AliasSpriteInfo final : public ISpriteInfo {
    public:
        AliasSpriteInfo(std::shared_ptr<PositionedSpriteInfo> positioned, std::shared_ptr<Sprite> aliasSprite)
                : _positioned(std::move(positioned)),
                  _aliasSprite(std::move(aliasSprite)) {}

        primitives::Point<unsigned int> getPosition() const noexcept override {
            return _positioned->getPosition();
        }

        bool isRotated() const noexcept override {
            return _positioned->isRotated();
        }

        const std::string &getName() const noexcept override {
            return _aliasSprite->getName();
        }

        primitives::Area getArea(bool trimmed) const noexcept override {
            return _aliasSprite->getArea(trimmed);
        }

        const std::shared_ptr<image::Image> &getImage() const noexcept override {
            return _aliasSprite->getImage();
        }

        bool isAlias() const noexcept override {
            return true;
        }

    private:
        std::shared_ptr<PositionedSpriteInfo> _positioned;
        std::shared_ptr<Sprite> _aliasSprite;
    };

}
