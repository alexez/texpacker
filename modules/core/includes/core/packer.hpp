#pragma once

#include <vector>
#include <string>
#include <map>
#include "sprite.hpp"
#include "primitives/primitives.hpp"
#include "core/result.hpp"
#include "max_rects/max_rects.hpp"

namespace core {

    struct Parameters {
        constexpr bool hasFixedSize() const noexcept {
            return size.width > 0 && size.height > 0 && !isSizeFinite;
        }

        constexpr bool isMaximum(unsigned width, unsigned height) const noexcept {
            return hasFixedSize() || (isSizeFinite && width >= size.width && height >= size.height);
        }

        bool POT{true};
        bool shouldRotate{true};
        primitives::Point<unsigned> padding{1u, 1u};
        primitives::Size size{8192u, 8192u};
        bool isSizeFinite{true};
    };

    class Packer {
    public:
        explicit Packer(Parameters parameters);
        ~Packer();

        void addSprite(std::shared_ptr<Sprite> sprite);
        std::shared_ptr<Result> getResult();

    private:
        std::shared_ptr<Result> getResult(rbp::MaxRects::Mode mode) const;
        primitives::Size getInitialSize(const std::vector<size_t> &rectIndices) const;
        Atlas getPreparedAtlas(const std::vector<primitives::Area> &resultRects, const std::vector<size_t> &resultIndices, primitives::Size size) const;
        void enlargeSize(primitives::Size &size) const;

        Parameters _parameters;
        std::vector<std::shared_ptr<Sprite>> _sprites;
        std::vector<std::shared_ptr<Sprite>> _emptySprites;
        std::vector<primitives::Size> _rects;
        std::map<std::string, Sprite *> _spriteHashMap;
        std::map<Sprite *, std::vector<std::shared_ptr<Sprite>>> _spriteAliasesMap;
    };

}
