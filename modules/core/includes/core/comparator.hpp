#pragma once

#include <string>

namespace core {

    template<typename ClassType, typename FieldType, FieldType field>
    struct Comparator {
        using is_transparent = void;

        bool operator()(const ClassType &a, const ClassType &b) const {
            return a.*field < b.*field;
        }

        bool operator()(const std::string &name, const ClassType &a) const {
            return name < a.*field;
        }

        bool operator()(const ClassType &a, const std::string &name) const {
            return a.*field < name;
        }
    };

}
