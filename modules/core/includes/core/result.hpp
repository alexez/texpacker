#pragma once

#include <vector>
#include "core/atlas.hpp"

namespace core {

    class Result {
    public:
        Result()
                : _atlases() {}

        void addAtlas(Atlas &&atlas) {
            _atlases.emplace_back(std::move(atlas));
        }

        Atlas &getAtlas(size_t index) {
            return _atlases[index];
        }

        size_t getAtlasesAmount() const {
            return _atlases.size();
        }

        auto begin() noexcept { return _atlases.begin().operator->(); }
        auto end() noexcept { return _atlases.end().operator->(); }
        auto begin() const noexcept { return _atlases.begin().operator->(); }
        auto end() const noexcept { return _atlases.end().operator->(); }

    private:
        std::vector<Atlas> _atlases;
    };

}
